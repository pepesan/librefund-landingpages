---
title: Instant payment
weight: 6
icon: icon6.svg
---

Every time a subscriber pays a subscription tier, you will receive the money immediately
