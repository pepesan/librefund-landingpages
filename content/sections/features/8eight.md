---
title: With a DAO in mind from the beginning 
weight: 8
icon: icon8.svg
---

We want decisions to be made in the fairest and most democratic way possible, so we will create a DAO so that all decisions about commissions or new features can be made by consensus with the community.
