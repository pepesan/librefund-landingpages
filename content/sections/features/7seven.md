---
title: Only 1% fee
weight: 7
icon: icon7.svg
---

To be a sustainable project we must have a commission, but we want it to be as low as possible, so we will start with only a 1% commission. And we will choose those blockchains that have the lowest commissions so that both subscribers and creators pay the minimum commissions possible.
