---
title: Multidevice
weight: 3
icon: icon3.svg
---

You and your supporter can access from any device, they only need a Metamask wallet
