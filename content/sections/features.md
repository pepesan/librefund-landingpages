---
title: Why do you choose LibreFund?
afterTitle: ...and so much more!
---

This DAO is originated for creators and by creators that knows how difficult is beginning, and we want to solve those problems.
<br><br>
Build a community and fund your project without frictions.
