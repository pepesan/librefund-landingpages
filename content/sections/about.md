---
title: LibreFund
---

LibreFund is a Decentralized Autonomous Organization (DAO) that wants to develop a simple and efficient method of financing projects, content creators, NGOs, and any type of social cause.
<br><br>
**Eliminate all the possible intermediaries in the donation process.**
