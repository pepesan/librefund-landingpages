---
title: Privacy Policy
---

LibreFund informs you about its Privacy Policy regarding the treatment and protection of personal data of users and customers that may be collected by browsing or contracting services through the website https://pepesan.gitlab.io/ librefund-landingpages/

In this sense, LibreFund guarantees compliance with current regulations on the protection of personal data, reflected in Organic Law 3/2018, of December 5, on the Protection of Personal Data and the Guarantee of Digital Rights (LOPD GDD). It also complies with Regulation (EU) 2016/679 of the European Parliament and of the Council of April 27, 2016 regarding the protection of natural persons (RGPD).

The use of the website implies acceptance of this Privacy Policy as well as the conditions included in the Legal Notice.

Identity of the person in charge

    Headline: David Vaquero Santiago
    NIF/CIF: 07977852A
    Address: C/ Finisterre 19 bajo. 37797 Castilians of Villiquera (Salamanca)
    Email: info@cursosdedesarrollo.com
    Website: https://cursosdedesarrollo.com

Principles applied in data processing

In the processing of your personal data, LibreFund will apply the following principles that comply with the requirements of the new European data protection regulation:

    Principle of legality, loyalty and transparency: LibreFund will always require consent for the processing of your personal data, which may be for one or more specific purposes about which you will be previously informed with absolute transparency.
    Principle of data minimization: CursosDeDearrollo will request only the data that is strictly necessary for the purpose or purposes for which it is requested.
    Principle of limitation of the term of conservation: The data will be kept for the time strictly necessary for the purpose or purposes of the treatment.
    LibreFund will inform you of the corresponding conservation period according to the purpose. In the case of subscriptions, the Holder will periodically review the lists and will eliminate those records that are inactive for a considerable time.
    Principle of integrity and confidentiality: Your data will be treated in such a way that its security, confidentiality and integrity is guaranteed. You should know that the Holder takes the necessary precautions to prevent unauthorized access or improper use of the data of its users by third parties.

Principles applied in data processing

In the processing of your personal data, LibreFund will apply the following principles that comply with the requirements of the new European data protection regulation:

    Principle of legality, loyalty and transparency: LibreFund will always require consent for the processing of your personal data, which may be for one or more specific purposes about which you will be previously informed with absolute transparency.
    Principle of data minimization: CursosDeDearrollo will request only the data that is strictly necessary for the purpose or purposes for which it is requested.
Collection of personal data

To browse https://cursosdedesarrollo.com you do not need to provide any personal data. The cases in which you do provide your personal data are the following:

DELETE THE ONES YOU DON'T USE:

    By contacting through the contact forms or sending an email.
    When making a comment on an article or page.
    By signing up for a subscription form or a newsletter that CursosDeDearrollo manages with MailChimp.


#### Website enhancements ####

This website uses the following packages from <a href="https://unpkg.com" rel="noopener" target="_blank" title="UNPKG">unpkg.com</a>:

- &dash; popper.js
- &dash; tippy.js

Using these packages, personal data may be transmitted.

### Your rights ###
The Owner informs you that you have the right to:

    Request access to stored data.
    Request a rectification or cancellation.
    Request the limitation of your treatment.
    Oppose the treatment.
    Request the portability of your data.

The exercise of these rights is personal and therefore must be exercised directly by the interested party, requesting it directly from CursosDeDevelopment, which means that any client, subscriber or collaborator who has provided their data at any time can contact the Owner and request information about the data. that you have stored and how you have obtained them, request their rectification, request the portability of your personal data, oppose the treatment, limit its use or request the cancellation of these data in the Holder's files.

To exercise your rights of access, rectification, cancellation, portability and opposition, you must send an email to info@cursosdedesarrollo.com together with the legally valid proof such as a photocopy of the D.N.I. or the equivalent.

You have the right to effective judicial protection and to file a claim with the control authority, in this case, the Spanish Data Protection Agency, if you consider that the processing of personal data that concerns you violates the Regulation.


Purpose of personal data processing

When you connect to the website to send an email to the Holder, you subscribe to his newsletter or make a contract, you are providing personal information for which the Holder is responsible. This information may include personal data such as your IP address, name and surname, physical address, email address, telephone number, and other information. By providing this information, you give your consent for your information to be collected, used, managed and stored by superadmin.es, only as described in the Legal Notice and in this Privacy Policy.

The personal data and the purpose of the treatment by the Holder is different depending on the information capture system:

DELETE THE ONES YOU DON'T USE:

    Contact forms: LibreFund requests personal data, among which may be: Name and surnames, email address, telephone number and address of your website in order to respond to your queries.
    For example, LibreFund uses this data to respond to your messages, questions, complaints, comments or concerns that you may have regarding the information included on the website, the services provided through the website, the treatment of your data personal information, questions regarding the legal texts included in the website, as well as any other query you may have that is not subject to the conditions of the website or the contract.
    Sweepstakes access forms: LibreFund requests the following personal data: Name and surnames, email address, to manage the list of raffle participants, send newsletters, promotions and special offers.
    The data you provide to the Holder will be located on the servers of The Rocket Science Group LLC d/b/a, domiciled in the USA. (MailChimp).
    Content subscription forms: CursosDeDearrollo requests the following personal data: Name and surnames, email address, telephone number and address of your website to manage the list of subscriptions, send newsletters, promotions and special offers.
    The data you provide to the Holder will be located on the servers of The Rocket Science Group LLC d/b/a, domiciled in the USA. (MailChimp).

There are other purposes for which LibreFund treats your personal data:

    To guarantee compliance with the conditions set forth in the Legal Notice and in the applicable law. This may include the development of tools and algorithms that help this website ensure the confidentiality of the personal data it collects.
    To support and improve the services offered by this website.
    To analyze the navigation. CursosDeDearrollo collects other non-identifying data that is obtained through the use of cookies that are downloaded to your computer when you browse the website, the characteristics and purpose of which are detailed in the Cookies Policy.
    To manage social networks. CursosDeDearrollo has a presence in social networks. If you become a follower on the Holder's social networks, the processing of personal data will be governed by this section, as well as by those conditions of use, privacy policies and access regulations that belong to the social network that is appropriate in each case and that you have previously accepted.

You can consult the privacy policies of the main social networks in these links:

    Twitter
    LinkedIn
    Youtube

CursosDeDearrollo will process your personal data in order to correctly manage its presence on the social network, inform you of its activities, products or services, as well as for any other purpose that the regulations of social networks allow.

In no case will LibreFund use the profiles of followers on social networks to send advertising individually.
Security of personal data

To protect your personal data, LibreFund takes all reasonable precautions and follows the best industry practices to prevent its loss, misuse, improper access, disclosure, alteration or destruction.

The website is hosted by Dinahosting. The security of your data is guaranteed, since they take all the necessary security measures for it. You can consult their privacy policy for more information.

Content from other websites

Pages on this website may include embedded content (eg, videos, images, articles, etc.). Embedded content from other websites behaves in exactly the same way as if you had visited the other website.

These websites may collect data about you, use cookies, embed additional third-party tracking code, and monitor your interaction using this code.
Cookies policy

For this website to work properly it needs to use cookies, which is information that is stored in your web browser.

On the Cookies Policy page you can consult all the information related to the collection policy, the purpose and the treatment of cookies.
Legitimation for data processing

The legal basis for the treatment of your data is: consent.

To contact the Owner, subscribe to a newsletter or make comments on this website you must accept this Privacy Policy.

Categories of personal data

The categories of personal data processed by the Holder are:

    Identification data.

Conservation of personal data

The personal data that you provide to the Holder will be kept until you request its deletion.
Recipients of personal data

    Mailchimp The Rocket Science Group LLC d/b/a, domiciled in the USA.
    You will find more information at: https://mailchimp.com
    The Rocket Science Group LLC d/b/a treats the data in order to provide its email services the Titulareting to the Owner.

You can also see a list of the types of cookies used by Google and its collaborators and all the information related to their use of advertising cookies.

Web navegation

When browsing the WEBSITE, non-identifying data may be collected, which may include IP address, geolocation, a record of how the services and sites are used, browsing habits and other data that cannot be used to identify you.

The website uses the following third party analytics services:

    Google Analytics

The Holder uses the information obtained to obtain statistical data, analyze trends, manage the site, study browsing patterns and to collect demographic information.
Accuracy and veracity of personal data

You agree that the data provided to the Owner is correct, complete, accurate and current, as well as keeping it duly updated.

As a User of the Website, you are solely responsible for the veracity and correctness of the data you send to the site, exonerating the Owner of any responsibility in this regard.

Aceptación y consentimiento

Como Usuario del sitio Web declaras haber sido informado de las condiciones sobre protección de datos de carácter personal, aceptas y consientes el tratamiento de los mismos por parte de el Titular en la forma y para las finalidades indicadas en esta Política de Privacidad.
Revocabilidad

Para ejercitar tus derechos de acceso, rectificación, cancelación, portabilidad y oposición tienes que enviar un correo electrónico a info@cursosdedesarrollo.com junto con la prueba válida en derecho como una fotocopia del D.N.I. o equivalente.

El ejercicio de tus derechos no incluye ningún dato que el Titular esté obligado a conservar con fines administrativos, legales o de seguridad.
Cambios en la Política de Privacidad

LibreFund se reserva el derecho a modificar la presente Política de Privacidad para adaptarla a novedades legislativas o jurisprudenciales, así como a prácticas de la industria.

Estas políticas estarán vigentes hasta que sean modificadas por otras debidamente publicadas.
